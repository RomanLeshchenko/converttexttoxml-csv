﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Managers
{
    public class Convert
    {
        public static XDocument ConvertToXml(string input)
        {
            if (!string.IsNullOrEmpty(input) && !string.IsNullOrWhiteSpace(input))
            {
                string[] wordsFromInput = input.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

                XDocument xdoc = new XDocument( new XDeclaration("1.0","utf-8","yes"));
                XElement text = new XElement("text");

                foreach (var word in wordsFromInput)
                {
                    text.Add(GetSingleSentence(word));
                }

                xdoc.Add(text);

                return xdoc;
            }

            return null;
        }

        public static string ConvertToCSV(XDocument xdoc)
        {
            string csvOut = string.Empty;
            StringBuilder sb = new StringBuilder(100000);
            int count = 0;
            foreach (XElement node in xdoc.Descendants("sentence"))
            {
                count++;

                sb.Append(string.Format("Sentence {0}, ", count));

                foreach (XElement innerNode in node.Elements())
                {
                    sb.AppendFormat("{0},", innerNode.Value);
                }
                sb.Remove(sb.Length - 1, 1);
                sb.AppendLine();
            }

            csvOut = sb.ToString();

            return csvOut;
        }

        private static XElement GetSingleSentence(string sentence)
        {
            string[] wordsFromSentence = sentence.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
            Array.Sort(wordsFromSentence);

            var xElement = AddSentenceToXml(wordsFromSentence);

            return xElement;

        }

        private static XElement AddSentenceToXml(string[] words)
        {
            XElement sentence = new XElement("sentence");

            foreach (var word in words)
            {
                XElement element = new XElement("word", word);
                sentence.Add(element);
            }

            return sentence;
        }
    }
}
