﻿$(document).ready(function () {
    $("#showCsv").click(function () {
        var inputText = $("#text").val();

        if (inputText.length != 0) {
            $.ajax({
                url: "http://localhost:49647/api/ConvertText/ParseTextToCSV/?data=" + inputText,
                type: "Get",
                success: function (data) {
                    $("#result").show();
                    $("#result").text(data);
                }
            })
        } else {
            alert("Please, enter text!");
        }
    });
});