﻿$(document).ready(function () {
    

    $("#showXml").click(function () {
        var inputText = $("#text").val();

        if (inputText.length != 0) {
            $.ajax({
                url: "http://localhost:49647/api/ConvertText/ConvertTextToXml/?value=" + inputText,
                type: "Get",
                datatype: "xml",
                contentType: "application/xml",
                success: function (data) {
                    var xmlstr = data.xml ? data.xml : (new XMLSerializer()).serializeToString(data);
                    $("#result").show();
                    $("#result").text(xmlstr);
                }
            })
        } else {
            alert("Please, enter text!");
        }
    });
});