﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Managers;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Net.Http.Headers;

namespace WebUI.Controllers
{
    [RoutePrefix("api/ConvertText")]
    public class ConvertTextController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage ConvertTextToXml(string value)
        {
            var xml = Managers.Convert.ConvertToXml(value);

            if (xml != null)
            {
                return new HttpResponseMessage()
                {
                    Content = new StringContent((
                    xml.Declaration + "\n" + xml.ToString()),
                    Encoding.UTF8,
                    "application/xml")
                };
            }

            return null;
        }

        [HttpGet]
        public HttpResponseMessage ParseTextToCSV(string data)
        {
            var xmlResult = Managers.Convert.ConvertToXml(data);

            if (xmlResult != null)
            {
                var csv = Managers.Convert.ConvertToCSV(xmlResult);
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

                result.Content = new StringContent(csv.ToString());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");

                return result;
            }

            return null;
        }
    }
}
